## Національний технічний університет України<br>“Київський політехнічний інститут ім. Ігоря Сікорського”

## Факультет прикладної математики<br>Кафедра системного програмування і спеціалізованих комп’ютерних систем


# Лабораторна робота №1<br>"Базова робота з git"

## КВ-11 Горбуль Андрій

## Хід виконання роботи

### 1. Зклонувати будь-який невеликий проєкт open-source з github
**Зклонувати двічі: `повний` репозиторій, а також `частковий`, що міститиме лише один коміт однієї гілки за вашим вибором.**

Обраний репозиторій: https://github.com/appwrite/appwrite

Клонування повного репозиторію:
```
$ git clone https://github.com/appwrite/appwrite.git
Cloning into 'appwrite'...
remote: Enumerating objects: 177273, done.
remote: Counting objects: 100% (21131/21131), done.
remote: Compressing objects: 100% (674/674), done.
remote: Total 177273 (delta 20760), reused 20516 (delta 20456), pack-reused 156142
Receiving objects: 100% (177273/177273), 246.60 MiB | 1.55 MiB/s, done.
Resolving deltas: 100% (130579/130579), done.
Updating files: 100% (28196/28196), done.
```

Клонування часткового репозиторію (гілка 1.3.x):
```
$ git clone https://github.com/appwrite/appwrite.git --depth=1 --single-branch --branch=1.3.x appwrite-1.3.x
Cloning into 'appwrite-1.3.x'...
remote: Enumerating objects: 11244, done.
remote: Counting objects: 100% (11244/11244), done.
remote: Compressing objects: 100% (3794/3794), done.
remote: Total 11244 (delta 7896), reused 9090 (delta 7087), pack-reused 0
Receiving objects: 100% (11244/11244), 69.13 MiB | 1.55 MiB/s, done.
Resolving deltas: 100% (7896/7896), done.
Updating files: 100% (24993/24993), done.
```
**Показати різницю в розмірі баз даних двох клонів**
```
$ du -sh ./*/.git
73M	    ./appwrite-1.3.x/.git
256M	./appwrite/.git
```

### 2. Зробити не менше трьох локальних комітів
- **Продемонструвати різні способи додавання файлів до індексу**
- **Продемонструвати різні способи вказання повідомлення коміту**

Модифікував файл `docker-compose.yml`, додав файли `cookie.jar` і `python.jpg`
```diff
$ git status
On branch 1.3.x
Your branch is up to date with 'origin/1.3.x'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
-        modified:   docker-compose.yml

Untracked files:
  (use "git add <file>..." to include in what will be committed)
-       cookie.jar
-       python.jpg

no changes added to commit (use "git add" and/or "git commit -a")
```

Коміт #1
```
$ git commit -a -m "Fixed docker file"
[1.3.x ad5fbc8] Fixed docker file
 1 file changed, 2 insertions(+), 2 deletions(-)
```
Оновлений статус, видно що зникло повідомлення про docker-compose.yml
```diff
$ git status
On branch 1.3.x
Your branch is ahead of 'origin/1.3.x' by 1 commit.
  (use "git push" to publish your local commits)

Untracked files:
  (use "git add <file>..." to include in what will be committed)
-       cookie.jar
-       python.jpg

nothing added to commit but untracked files present (use "git add" to track)
```
Коміт #2
```
$ git add cookie.jar 
$ git commit -m "Coffee included"
[1.3.x 63b9bd3] Coffee included
 1 file changed, 19 insertions(+)
 create mode 100644 cookie.jar
```
Коміт #3
```
$ git add python.jpg 
$ git commit -m "Important feature"
[1.3.x 3a29c5a] Important feature
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 python.jpg
```
Оновлений статус, видно що ми створили 3 нових коміти
```
$ git status
On branch 1.3.x
Your branch is ahead of 'origin/1.3.x' by 3 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```
Повідомлення коміту також можна написати в консольному редакторі<br>(якщо не вказати опцію -m)
```
GNU nano 4.8                                                         
COMMIT_EDITMSG                                                                   
some msg
# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
#
# On branch 1.3.x
# Your branch is ahead of 'origin/1.3.x' by 3 commits.
#   (use "git push" to publish your local commits)
```

### 3. Продемонструвати уміння вносити зміни до останнього коміту за допомогою опції `--amend`
Поточний стан гілки
```
$ git log --oneline 
3a29c5a (HEAD -> 1.3.x) Important feature
63b9bd3 Coffee included
ad5fbc8 Fixed docker file
125c14d (grafted, origin/1.3.x) Merge branch 'master' of https://github.com/appwrite/appwrite into 1.3.x
```
Використання опції `--amend`
```
$ git commit --amend -m "Important feature --amend"
[1.3.x 25a6552] Important feature --amend
 Date: Fri Oct 6 16:51:54 2023 +0300
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 python.jpg
$ git log --oneline 
25a6552 (HEAD -> 1.3.x) Important feature --amend
63b9bd3 Coffee included
ad5fbc8 Fixed docker file
125c14d (grafted, origin/1.3.x) Merge branch 'master' of https://github.com/appwrite/appwrite into 1.3.x
```
Видно що змінилися назва і хеш останнього коміту
### 4. Продемонструвати уміння об'єднати кілька останніх комітів в один за допомогою `git reset`
Додав два нових коміти
```
$ git log --oneline 
145c649 (HEAD -> 1.3.x) modified .gitignore
37c7844 1.txt
25a6552 Important feature --amend
63b9bd3 Coffee included
ad5fbc8 Fixed docker file
125c14d (grafted, origin/1.3.x) Merge branch 'master' of https://github.com/appwrite/appwrite into 1.3.x
```
Відкатив на 3 коміти
```diff
$ git reset HEAD~3
Unstaged changes after reset:
M       .gitignore
$ git status
On branch 1.3.x
Your branch is ahead of 'origin/1.3.x' by 2 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
-       modified:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
-       1.txt
-       python.jpg

no changes added to commit (use "git add" and/or "git commit -a")
$ git log --oneline 
63b9bd3 (HEAD -> 1.3.x) Coffee included
ad5fbc8 Fixed docker file
125c14d (grafted, origin/1.3.x) Merge branch 'master' of https://github.com/appwrite/appwrite into 1.3.x
```
Об'єднав зміни трьох останніх комітів у один
```
$ git add .
$ git commit -m "One big commit"
[1.3.x 26a96c4] One big commit
 3 files changed, 3 insertions(+)
 create mode 100644 1.txt
 create mode 100644 python.jpg
 ```
### 5. Видалити файл(и) одним способом на вибір.
Видалив файл 1.txt
```
$ git rm 1.txt 
rm '1.txt'
$ git commit -m "1.txt removed"
[1.3.x 5f67b4b] 1.txt removed
 1 file changed, 0 insertions(+), 0 deletions(-)
 delete mode 100644 1.txt
```
### 6. Перемістити файл(и) одним способом на вибір.
Переіменував python.jpg у based.jpg
```
$ git mv python.jpg based.jpg
$ git commit -m "Renamed.jpg"
[1.3.x 27baab0] Renamed.jpg
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename python.jpg => based.jpg (100%)
```
### 7. Гілкування:
- ### 1. Створити три гілки, принаймні з одним унікальним комітом кожна
- ### 2. Показати уміння переключатися між гілками
Створив 3 гілки: branch0, branch1, branch2
```
$ git branch branch0
$ git branch branch1
$ git branch branch2
$ git branch
* 1.3.x
  branch0
  branch1
  branch2
```
Переключився на гілку branch0, створив файл і закомітив
```
$ git checkout branch0
Switched to branch 'branch0'
$ touch file0
$ git add .
$ git commit -m "commit0"
[branch0 b0739ac] commit0
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 file0
```
Аналогічно зробив з гілками branch1, branch2
```
git log --oneline --all
092d86f (branch2) commit2
90471d5 (branch1) commit1
b0739ac (branch0) commit0
27baab0 (HEAD -> 1.3.x) Renamed.jpg
5f67b4b 1.txt removed
26a96c4 One big commit
63b9bd3 Coffee included
ad5fbc8 Fixed docker file
125c14d (grafted, origin/1.3.x) Merge branch 'master' of https://github.com/appwrite/appwrite into 1.3.x
```
### 8. Продемонструвати уміння знайти в історії комітів набір комітів, в яких була зміна по конкретному шаблону в конкретному файлі.
Перегляд історії останніх 5-ти комітів
```
$ git log -n 5 --oneline 
8a4302ffe (HEAD -> main, origin/main, origin/HEAD) Merge pull request #6454 from appwrite/hacktoberfest-update
4a43c84b2 (origin/hacktoberfest-update) Update CONTRIBUTING.md
897e975b4 Update with hacktoberfest instructions.
514a5c37f Merge pull request #6253 from appwrite/feat-package-json
edf06962e Merge pull request #6350 from appwrite/update-references
```
Перегляд історії з вказанням діапазону і шаблоном форматування
```
$ git log edf06962e..HEAD --format="%h %as '%s'" 
8a4302ffe 2023-10-04 'Merge pull request #6454 from appwrite/hacktoberfest-update'
4a43c84b2 2023-10-03 'Update CONTRIBUTING.md'
897e975b4 2023-10-03 'Update with hacktoberfest instructions.'
514a5c37f 2023-10-02 'Merge pull request #6253 from appwrite/feat-package-json'
c1b0ef206 2023-09-15 'chore: remove unused props from package.json'
c58f5a452 2023-09-15 'chore: add package.json'
```
Пошук в історії комітів в яких змінювався текст `'Appwrite is an end-to-end backend server for'`
```
$ git log --oneline  -G 'Appwrite is an end-to-end backend server for' README.md
52d74917a Updated README.md
5c8a063eb Update README.md
129d9d90b Updated docs
```
Перегляд змін у файлі README.md коміта 52d74917a
````diff
$ git diff 52d74917a README.md
diff --git a/README.md b/README.md
index 3eb96ad9f..605cb24be 100644
--- a/README.md
+++ b/README.md
@@ -1,27 +1,47 @@
+> Great news! Appwrite Cloud is now in public beta! Sign up at [cloud.appwrite.io](https://cloud.appwrite.io) for a hassle-free, hosted experience. Join us in the Cloud today! ☁️🎉
+
+<br />
 <p align="center">
-    <a href="https://appwrite.io" target="_blank"><img width="260" height="39" src="https://appwrite.io/images/github-logo.png" alt="Appwrite Logo"></a>
+    <a href="https://appwrite.io" target="_blank"><img src="./public/images/banner.png" alt="Appwrite Logo"></a>
     <br />
     <br />
-    <b>A complete backend solution for your [Flutter / Vue / Angular / React / iOS / Android / *ANY OTHER*] app</b>
+    <b>Appwrite is a backend platform for developing Web, Mobile, and Flutter applications. Built with the open source community and optimized for developer experience in the coding languages you love.</b>
     <br />
     <br />
 </p>
 
-[![Hacktoberfest](https://badgen.net/badge/hacktoberfest/friendly/pink)](CONTRIBUTING.md)
-[![Discord](https://img.shields.io/discord/564160730845151244?label=discord)](https://discord.gg/GSeTUeA)
-[![Docker Pulls](https://badgen.net/docker/pulls/appwrite/appwrite)](https://travis-ci.org/appwrite/appwrite)
-[![Travis CI](https://badgen.net/travis/appwrite/appwrite?label=build)](https://travis-ci.org/appwrite/appwrite)
-[![Twitter Account](https://badgen.net/twitter/follow/appwrite_io?label=twitter)](https://twitter.com/appwrite_io)
-[![Follow Appwrite on StackShare](https://badgen.net/badge/follow%20on/stackshare/blue)](https://stackshare.io/appwrite)
+
+<!-- [![Build Status](https://img.shields.io/travis/com/appwrite/appwrite?style=flat-square)](https://travis-ci.com/appwrite/appwrite) -->
+
+[![We're Hiring](https://img.shields.io/static/v1?label=We're&message=Hiring&color=blue&style=flat-square)](https://appwrite.io/company/careers)
+[![Hacktoberfest](https://img.shields.io/static/v1?label=hacktoberfest&message=ready&color=191120&style=flat-square)](https://hacktoberfest.appwrite.io)
+[![Discord](https://img.shields.io/discord/564160730845151244?label=discord&style=flat-square)](https://appwrite.io/discord?r=Github)
+[![Build Status](https://img.shields.io/github/actions/workflow/status/appwrite/appwrite/tests.yml?branch=master&label=tests&style=flat-square)](https://github.com/appwrite/appwrite/actions)
+[![Twitter Account](https://img.shields.io/twitter/follow/appwrite?color=00acee&label=twitter&style=flat-square)](https://twitter.com/appwrite)
+
+<!-- [![Docker Pulls](https://img.shields.io/docker/pulls/appwrite/appwrite?color=f02e65&style=flat-square)](https://hub.docker.com/r/appwrite/appwrite) -->
+<!-- [![Translate](https://img.shields.io/badge/translate-f02e65?style=flat-square)](docs/tutorials/add-translations.md) -->
+<!-- [![Swag Store](https://img.shields.io/badge/swag%20store-f02e65?style=flat-square)](https://store.appwrite.io) -->
+
+English | [简体中文](README-CN.md)
+
+[**Announcing Appwrite Cloud Public Beta! Sign up today!**](https://cloud.appwrite.io)
 
 Appwrite is an end-to-end backend server for Web, Mobile, Native, or Backend apps packaged as a set of Docker<nobr> microservices. Appwrite abstracts the complexity and repetitiveness required to 
build a modern backend API from scratch and allows you to build secure apps faster.
 
-Using Appwrite, you can easily integrate your app with user authentication & multiple sign-in methods, a database for storing and querying users and team data, storage and file management, image m
anipulation, schedule CRON tasks, and [more services](https://appwrite.io/docs).
+Using Appwrite, you can easily integrate your app with user authentication and multiple sign-in methods, a database for storing and querying users and team data, storage and file management, image
 manipulation, Cloud Functions, and [more services](https://appwrite.io/docs).
 
-Find out more at: [https://appwrite.io](https://appwrite.io)
+<p align="center">
+    <br />
+    <a href="https://www.producthunt.com/posts/appwrite-2?utm_source=badge-top-post-badge&utm_medium=badge&utm_souce=badge-appwrite-2" target="_blank"><img src="https://api.producthunt.com/widgets
/embed-image/v1/top-post-badge.svg?post_id=360315&theme=light&period=daily" alt="Appwrite - 100&#0037;&#0032;open&#0032;source&#0032;alternative&#0032;for&#0032;Firebase | Product Hunt" style="widt
h: 250px; height: 54px;" width="250" height="54" /></a>
+    <br />
+    <br />
+</p>
 
 ![Appwrite](public/images/github.png)
 
+Find out more at: [https://appwrite.io](https://appwrite.io)
+
 Table of Contents:
 
 - [Installation](#installation)
@@ -30,30 +50,33 @@ Table of Contents:
     - [CMD](#cmd)
     - [PowerShell](#powershell)
   - [Upgrade from an Older Version](#upgrade-from-an-older-version)
+- [One-Click Setups](#one-click-setups)
 - [Getting Started](#getting-started)
   - [Services](#services)
   - [SDKs](#sdks)
     - [Client](#client)
     - [Server](#server)
+    - [Community](#community)
+- [Architecture](#architecture)
+- [Contributing](#contributing)
 - [Security](#security)
 - [Follow Us](#follow-us)
-- [Contributing](#contributing)
 - [License](#license)
-      
+
 ## Installation
 
-Appwrite backend server is designed to run in a container environment. Running your server is as easy as running one command from your terminal. You can either run Appwrite on your localhost using
 docker-compose or on any other container orchestration tool like Kubernetes, Docker Swarm, or Rancher.
+Appwrite is designed to run in a containerized environment. Running your server is as easy as running one command from your terminal. You can either run Appwrite on your localhost using docker-com
pose or on any other container orchestration tool, such as Kubernetes, Docker Swarm, or Rancher.
 
-The easiest way to start running your Appwrite server is by running our docker-compose file. Before running the installation command make sure you have [Docker](https://www.docker.com/products/doc
ker-desktop) installed on your machine:
+The easiest way to start running your Appwrite server is by running our docker-compose file. Before running the installation command, make sure you have [Docker](https://www.docker.com/products/do
cker-desktop) installed on your machine:
 
 ### Unix
 
 ```bash
 docker run -it --rm \
     --volume /var/run/docker.sock:/var/run/docker.sock \
-    --volume "$(pwd)"/appwrite:/install/appwrite:rw \
-    -e version=0.6.2 \
-    appwrite/install
+    --volume "$(pwd)"/appwrite:/usr/src/code/appwrite:rw \
+    --entrypoint="install" \
+    appwrite/appwrite:1.4.3
 ```
 
 ### Windows
@@ -63,84 +86,134 @@ docker run -it --rm \
 ```cmd
 docker run -it --rm ^
     --volume //var/run/docker.sock:/var/run/docker.sock ^
-    --volume "%cd%"/appwrite:/install/appwrite:rw ^
-    -e version=0.6.2 ^
-    appwrite/install
+    --volume "%cd%"/appwrite:/usr/src/code/appwrite:rw ^
+    --entrypoint="install" ^
+    appwrite/appwrite:1.4.3
 ```
 
 #### PowerShell
 
 ```powershell
-docker run -it --rm ,
-    --volume /var/run/docker.sock:/var/run/docker.sock ,
-    --volume ${pwd}/appwrite:/install/appwrite:rw ,
-    -e version=0.6.2 ,
-    appwrite/install
+docker run -it --rm `
+    --volume /var/run/docker.sock:/var/run/docker.sock `
+    --volume ${pwd}/appwrite:/usr/src/code/appwrite:rw `
+    --entrypoint="install" `
+    appwrite/appwrite:1.4.3
 ```
 
-Once the Docker installation completes, go to http://localhost to access the Appwrite console from your browser. Please note that on non-linux native hosts, the server might take a few minutes to 
start after installation completes.
-
+Once the Docker installation is complete, go to http://localhost to access the Appwrite console from your browser. Please note that on non-Linux native hosts, the server might take a few minutes t
o start after completing the installation.
 
-For advanced production and custom installation, check out our Docker [environment variables](docs/tutorials/environment-variables.md) docs. You can also use our public [docker-compose.yml](https:
//appwrite.io/docker-compose.yml) file to manually set up and environment.
+For advanced production and custom installation, check out our Docker [environment variables](https://appwrite.io/docs/environment-variables) docs. You can also use our public [docker-compose.yml]
(https://appwrite.io/install/compose) and [.env](https://appwrite.io/install/env) files to manually set up an environment.
 
 ### Upgrade from an Older Version
 
 If you are upgrading your Appwrite server from an older version, you should use the Appwrite migration tool once your setup is completed. For more information regarding this, check out the [Instal
lation Docs](https://appwrite.io/docs/installation).
 
+## One-Click Setups
+
+In addition to running Appwrite locally, you can also launch Appwrite using a pre-configured setup. This allows you to get up and running quickly with Appwrite without installing Docker on your lo
cal machine.
+
+Choose from one of the providers below:
+
+<table border="0">
+  <tr>
+    <td align="center" width="100" height="100">
+      <a href="https://marketplace.digitalocean.com/apps/appwrite">
+        <img width="50" height="39" src="public/images/integrations/digitalocean-logo.svg" alt="DigitalOcean Logo" />
+          <br /><sub><b>DigitalOcean</b></sub></a>
+        </a>
+    </td>
+    <td align="center" width="100" height="100">
+      <a href="https://gitpod.io/#https://github.com/appwrite/integration-for-gitpod">
+        <img width="50" height="39" src="public/images/integrations/gitpod-logo.svg" alt="Gitpod Logo" />
+          <br /><sub><b>Gitpod</b></sub></a>    
+      </a>
+    </td>
+    <td align="center" width="100" height="100">
+      <a href="https://www.linode.com/marketplace/apps/appwrite/appwrite/">
+        <img width="50" height="39" src="public/images/integrations/akamai-logo.svg" alt="Akamai Logo" />
+          <br /><sub><b>Akamai Compute</b></sub></a>    
+      </a>
+    </td>
+  </tr>
+</table>
+
 ## Getting Started
 
-Getting started with Appwrite is as easy as creating a new project, choosing your platform, and integrating its SDK in your code. You can easily get started with your platform of choice by reading
 one of our Getting Started tutorials.
+Getting started with Appwrite is as easy as creating a new project, choosing your platform, and integrating its SDK into your code. You can easily get started with your platform of choice by readi
ng one of our Getting Started tutorials.
 
-* [Getting Started for Web](https://appwrite.io/docs/getting-started-for-web)
-* [Getting Started for Flutter](https://appwrite.io/docs/getting-started-for-flutter)
-* [Getting Started for Server](https://appwrite.io/docs/getting-started-for-server)
-* Getting Started for Android (Coming soon...)
-* Getting Started for iOS (Coming soon...)
+- [Getting Started for Web](https://appwrite.io/docs/getting-started-for-web)
+- [Getting Started for Flutter](https://appwrite.io/docs/getting-started-for-flutter)
+- [Getting Started for Apple](https://appwrite.io/docs/getting-started-for-apple)
+- [Getting Started for Android](https://appwrite.io/docs/getting-started-for-android)
+- [Getting Started for Server](https://appwrite.io/docs/getting-started-for-server)
+- [Getting Started for CLI](https://appwrite.io/docs/command-line)
 
 ### Services
 
-* [**Account**](https://appwrite.io/docs/client/account) - Manage current user authentication and account. Track and manage the user sessions, devices, sign-in methods, and security logs.
-* [**Users**](https://appwrite.io/docs/server/users) - Manage and list all project users when in admin mode.
-* [**Teams**](https://appwrite.io/docs/client/teams) - Manage and group users in teams. Manage memberships, invites, and user roles within a team.
-* [**Database**](https://appwrite.io/docs/client/database) - Manage database collections and documents. Read, create, update, and delete documents and filter lists of documents collections using a
n advanced filter with graph-like capabilities.
-* [**Storage**](https://appwrite.io/docs/client/storage) - Manage storage files. Read, create, delete, and preview files. Manipulate the preview of your files to fit your app perfectly. All files 
are scanned by ClamAV and stored in a secure and encrypted way.
-* [**Locale**](https://appwrite.io/docs/client/locale) - Track your user's location, and manage your app locale-based data.
-* [**Avatars**](https://appwrite.io/docs/client/avatars) - Manage your users' avatars, countries' flags, browser icons, credit card symbols, and generate QR codes.
+- [**Account**](https://appwrite.io/docs/references/cloud/client-web/account) - Manage current user authentication and account. Track and manage the user sessions, devices, sign-in methods, and se
curity logs.
+- [**Users**](https://appwrite.io/docs/server/users) - Manage and list all project users when building backend integrations with Server SDKs.
+- [**Teams**](https://appwrite.io/docs/references/cloud/client-web/teams) - Manage and group users in teams. Manage memberships, invites, and user roles within a team.
+- [**Databases**](https://appwrite.io/docs/references/cloud/client-web/databases) - Manage databases, collections, and documents. Read, create, update, and delete documents and filter lists of doc
ument collections using advanced filters.
+- [**Storage**](https://appwrite.io/docs/references/cloud/client-web/storage) - Manage storage files. Read, create, delete, and preview files. Manipulate the preview of your files to perfectly fit
 your app. All files are scanned by ClamAV and stored in a secure and encrypted way.
+- [**Functions**](https://appwrite.io/docs/server/functions) - Customize your Appwrite server by executing your custom code in a secure, isolated environment. You can trigger your code on any Appw
rite system event either manually or using a CRON schedule.
+- [**Realtime**](https://appwrite.io/docs/realtime) - Listen to real-time events for any of your Appwrite services including users, storage, functions, databases, and more.
+- [**Locale**](https://appwrite.io/docs/references/cloud/client-web/locale) - Track your user's location and manage your app locale-based data.
+- [**Avatars**](https://appwrite.io/docs/references/cloud/client-web/avatars) - Manage your users' avatars, countries' flags, browser icons, and credit card symbols. Generate QR codes from links o
r plaintext strings.
 
 For the complete API documentation, visit [https://appwrite.io/docs](https://appwrite.io/docs). For more tutorials, news and announcements check out our [blog](https://medium.com/appwrite-io) and 
[Discord Server](https://discord.gg/GSeTUeA).
 
 ### SDKs
 
-Below is a list of currently supported platforms and languages. If you wish to help us add support to your platform of choice, you can go over to our [SDK Generator](https://github.com/appwrite/sd
k-generator) project and view our [contribution guide](https://github.com/appwrite/sdk-generator/blob/master/CONTRIBUTING.md).
+Below is a list of currently supported platforms and languages. If you would like to help us add support to your platform of choice, you can go over to our [SDK Generator](https://github.com/appwr
ite/sdk-generator) project and view our [contribution guide](https://github.com/appwrite/sdk-generator/blob/master/CONTRIBUTING.md).
 
 #### Client
-* ✅ [Web](https://github.com/appwrite/sdk-for-js) (Maintained by the Appwrite Team)
-* ✅ [Flutter](https://github.com/appwrite/sdk-for-flutter) (Maintained by the Appwrite Team)
+
+- ✅ &nbsp; [Web](https://github.com/appwrite/sdk-for-web) (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Flutter](https://github.com/appwrite/sdk-for-flutter) (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Apple](https://github.com/appwrite/sdk-for-apple) - **Beta** (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Android](https://github.com/appwrite/sdk-for-android) (Maintained by the Appwrite Team)
 
 #### Server
-* ✅ [NodeJS](https://github.com/appwrite/sdk-for-node) (Maintained by the Appwrite Team)
-* ✅ [PHP](https://github.com/appwrite/sdk-for-php) (Maintained by the Appwrite Team)
-* ✅ [Deno](https://github.com/appwrite/sdk-for-deno) - **Beta** (Maintained by the Appwrite Team)
-* ✅ [Ruby](https://github.com/appwrite/sdk-for-ruby) - **Beta** (Maintained by the Appwrite Team)
-* ✅ [Python](https://github.com/appwrite/sdk-for-python) - **Beta** (Maintained by the Appwrite Team)
-* ✅ [Go](https://github.com/appwrite/sdk-for-go) **Work in progress** (Maintained by the Appwrite Team)
-* ✅ [Dart](https://github.com/appwrite/sdk-for-dart) **Work in progress** (Maintained by the Appwrite Team)
+
+- ✅ &nbsp; [NodeJS](https://github.com/appwrite/sdk-for-node) (Maintained by the Appwrite Team)
+- ✅ &nbsp; [PHP](https://github.com/appwrite/sdk-for-php) (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Dart](https://github.com/appwrite/sdk-for-dart) - (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Deno](https://github.com/appwrite/sdk-for-deno) - **Beta** (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Ruby](https://github.com/appwrite/sdk-for-ruby) (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Python](https://github.com/appwrite/sdk-for-python) (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Kotlin](https://github.com/appwrite/sdk-for-kotlin) - **Beta** (Maintained by the Appwrite Team)
+- ✅ &nbsp; [Apple](https://github.com/appwrite/sdk-for-apple) - **Beta** (Maintained by the Appwrite Team)
+- ✅ &nbsp; [.NET](https://github.com/appwrite/sdk-for-dotnet) - **Experimental** (Maintained by the Appwrite Team)
+
+#### Community
+
+- ✅ &nbsp; [Appcelerator Titanium](https://github.com/m1ga/ti.appwrite) (Maintained by [Michael Gangolf](https://github.com/m1ga/))
+- ✅ &nbsp; [Godot Engine](https://github.com/GodotNuts/appwrite-sdk) (Maintained by [fenix-hub @GodotNuts](https://github.com/fenix-hub))
 
 Looking for more SDKs? - Help us by contributing a pull request to our [SDK Generator](https://github.com/appwrite/sdk-generator)!
 
+## Architecture
+
+![Appwrite Architecture](docs/specs/overview.drawio.svg)
+
+Appwrite uses a microservices architecture that was designed for easy scaling and delegation of responsibilities. In addition, Appwrite supports multiple APIs, such as REST, WebSocket, and GraphQL
 to allow you to interact with your resources by leveraging your existing knowledge and protocols of choice.
+
+The Appwrite API layer was designed to be extremely fast by leveraging in-memory caching and delegating any heavy-lifting tasks to the Appwrite background workers. The background workers also allo
w you to precisely control your compute capacity and costs using a message queue to handle the load. You can learn more about our architecture in the [contribution guide](CONTRIBUTING.md#architectu
re-1).
+
 ## Contributing
 
-All code contributions - including those of people having commit access - must go through a pull request and approved by a core developer before being merged. This is to ensure proper review of all the code.
+All code contributions, including those of people having commit access, must go through a pull request and be approved by a core developer before being merged. This is to ensure a proper review of all the code.
 
 We truly ❤️ pull requests! If you wish to help, you can learn more about how you can contribute to this project in the [contribution guide](CONTRIBUTING.md).
 
 ## Security
 
-For security issues, kindly email us at [security@appwrite.io](mailto:security@appwrite.io) instead of posting a public issue in GitHub.
+For security issues, kindly email us at [security@appwrite.io](mailto:security@appwrite.io) instead of posting a public issue on GitHub.
 
 ## Follow Us
 
-Join our growing community around the world! See our official [Blog](https://medium.com/appwrite-io). Follow us on [Twitter](https://twitter.com/appwrite_io), [Facebook Page](https://www.facebook.com/appwrite.io), [Facebook Group](https://www.facebook.com/groups/appwrite.developers/) or join our live [Discord server](https://discord.gg/GSeTUeA) for more help, ideas, and discussions.
+Join our growing community around the world! Check out our official [Blog](https://medium.com/appwrite-io). Follow us on [Twitter](https://twitter.com/appwrite), [Facebook Page](https://www.facebook.com/appwrite.io), [Facebook Group](https://www.facebook.com/groups/appwrite.developers/), [Dev Community](https://dev.to/appwrite) or join our live [Discord server](https://discord.gg/GSeTUeA) for more help, ideas, and discussions.
 
 ## License
 
(END)
````